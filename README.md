### Run:
* `docker-compose up --build`

* `docker-compose run --rm php sh -lc 'COMPOSER_MEMORY_LIMIT=-1 composer install'`

* `docker exec -it employee-service_node_1 npm run migrate`

* `open localhost:3000`
