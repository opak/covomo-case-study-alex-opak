var EmployeeRepository = require('../repositories/employee');

class EmployeeForm {
  constructor(employee, error){
    this.employee = employee.dataValues || {};
    this.transformErrors(error.errors || []);
    this.positions = this.createPositions();
    this.selectedPosition = this.selectPosition(this.employee.position);
    this.filteredPositions = this.filterPositions();
  }

  transformErrors(errors) {
    this.errors = errors.reduce((accumulator, currentValue) => {
      accumulator[currentValue.path] = currentValue.message;
      return accumulator;
    }, {});
  }

  createPositions(){
    return EmployeeRepository.positions();
  }

  filterPositions(){
    const selectedPosition = this.selectedPosition.value;
    return this.positions.filter(position => position.value !== selectedPosition);
  }

  selectPosition(position){
    if(position === undefined){
      return {};
    }
    return this.positions.find(obj => obj.value === parseInt(position));
  }
}

module.exports = EmployeeForm;
