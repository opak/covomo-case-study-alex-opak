'use strict';
const {
  Model
} = require('sequelize');

const validPositions = [0, 1, 2, 3];

module.exports = (sequelize, DataTypes) => {
  class Employee extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Employee.init({
    firstName:  {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          len: {
            args: [2],
            msg: "Please enter First Name"
          }
        },
        notNull: {
          msg: "Please enter First Name"
        }
      }
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          len: {
            args: [2],
            msg: "Please enter Last Name"
          }
        },
        notNull: {
          msg: "Please enter Last Name"
        }
      }
    },
    position: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        isInt: {
          msg: "Please select Position"
        },
        isIn: {
          args: [validPositions],
          msg: "Choose position from list"
        },
        notNull: {
          msg: "Please select Position"
        }
      }
    },
    hoursWorked: {
      type: DataTypes.INTEGER,
      allowNull: true,
      validate: {
        isInt: {
          msg: "Worked hours must be a number"
        }
      }
    }
  }, {
    sequelize,
    validPositions: validPositions,
    modelName: 'Employee',
  });
  return Employee;
};
