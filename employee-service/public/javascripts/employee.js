$( document ).ready(function() {
  var positionSelector = '#employeeForm #position';
  var hourseWorkedSelector = '#employeeForm #hoursWorked'
  $(positionSelector).on('change', function(el) {
    toggleHourseWorkedField();
  });

  var toggleHourseWorkedField = function(){
    var option = $(`${positionSelector} option:selected`);
    var showHoursField = option.attr('data-hours');
    if (showHoursField === "true"){
      $(hourseWorkedSelector).parent().css("display", '');
    }
    else {
      $(hourseWorkedSelector).parent().css("display", "none");
    }
  }

  toggleHourseWorkedField();

  $('.confirmation').on('click', function () {
    return confirm('User will be deleted permanently. Are you sure?');
  });
});
