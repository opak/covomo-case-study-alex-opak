var Employee = require('../models').Employee;

class EmployeeRepository{
  static new(params) {
    this.prepareParams(params);
    const employee = Employee.build(params);
    return employee;
  }

  static update(id, params){
    this.prepareParams(params);
    return this.find(id).then(employee => {
      return employee.update(params);
    });
  }

  static find(id) {
    return Employee.findByPk(id);
  }

  static all() {
    return Employee.findAll();
  }

  static destroy(id) {
    return this.find(id).then(employee => {
      return employee.destroy();
    });
  }

  static positions(){
    return [
      { value: 0, title: "CEO", hoursRequired: false },
      { value: 1, title: "CTO", hoursRequired: false },
      { value: 2, title: "Head of Marketing", hoursRequired: false },
      { value: 3, title: "Freelancer", hoursRequired: true }
    ]
  }

  static prepareParams(params) {
    if(params.position){
      params.position = parseInt(params.position);
    }
    this.prepareHouseWorked(params);
  }

  static prepareHouseWorked(params){
    if (this.addHoursWorked(params.position)) {
      params.hoursWorked = parseInt(params.hoursWorked);
    }
    else {
      delete params.hoursWorked;
    }
  }

  static addHoursWorked(position){
    const positionObj = this.positions().find(element => element.value === position);
    if (!positionObj){ return false; }
    return positionObj.hoursRequired
  }
}

module.exports = EmployeeRepository;
