var express = require('express');
var router = express.Router();
var EmployeeForm = require('../forms/employee');
var EmployeeRepository = require('../repositories/employee');
var SalaryService = require('../services/salary');

const renderNewForm = (res, employee, error) => {
  const formObject = new EmployeeForm(employee, error)
  res.render('employees/new', {
    form: {
      action: "/employees/create",
      method: "POST",
    },
    formObject: formObject,
    isEmployeesRoute: true,
    isNew: true
  });
};

const renderEditForm = (res, employee, error) => {
  const formObject = new EmployeeForm(employee, error)
  res.render('employees/edit', {
    form: {
      action: `/employees/${employee.id}/update`,
      method: "POST",
    },
    formObject: formObject,
    isEmployeesRoute: true,
    isNew: false
  });
};

/* GET list of employees */
router.get('/', function(req, res, next) {
  EmployeeRepository.all().then(employees => {
    res.render('employees/index', { isEmployeesRoute: true, collection: employees });
  }, () => {
    res.redirect('/');
  });
});

/* GET create form */
router.get('/new', function(req, res, next) {
  renderNewForm(res, {}, {});
});

/* POST create */
router.post('/create', function(req, res, next) {
  const employee = EmployeeRepository.new(req.body);
  employee.validate().then((employee) => {
    employee.save().then(() => {
      res.redirect('/employees');
    }, (error) => {
      renderNewForm(res, employee, error);
    });
  }, (error) => {
    renderNewForm(res, employee, error);
  });
});

/* GET edit form */
router.get('/:id/edit', function(req, res, next) {
  EmployeeRepository.find(req.params.id).then(employee => {
    if(employee === null) {
      res.redirect('/employees');
    }
    else {
      renderEditForm(res, employee, {});
    }
  });
});

/* POST update */
router.post('/:id/update', function(req, res, next) {
  EmployeeRepository.update(req.params.id, req.body).then((employee) => {
    res.redirect('/employees');
  }, (error) => {
    renderEditForm(res, employee, error);
  });
});

/* GET employee */
router.get('/:id', function(req, res, next) {
  EmployeeRepository.find(req.params.id).then(employee => {
    if(employee === null) {
      res.redirect('/employees');
    }
    else {
      SalaryService.get(employee.position, employee.hoursWorked).then(position =>{
        res.render('employees/show', {
          employee: employee,
          position: position,
          isEmployeesRoute: true
        });
      })
    }
  });
});

/* GET destroy employee */
router.get('/:id/destroy', function(req, res, next) {
  EmployeeRepository.destroy(req.params.id).then(() => {
    res.redirect('/employees');
  }, () => {
    res.redirect('/employees');
  });
});

module.exports = router;
