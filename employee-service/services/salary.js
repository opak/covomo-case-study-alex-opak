var request = require('request');

var EmployeeRepository = require('../repositories/employee');

class SalaryService {
  static get(position, hours) {
    const pos = EmployeeRepository.positions().find(p => p.value === position);
    const url = `${this.host()}/api/salary`;
    var query = { position: pos.title };
    if (pos.hoursRequired) {
      query.hours = hours
    }
    return this.makeRequest({ uri: url, qs: query, json: true }, pos);
  }

  static makeRequest(params, position){
    return new Promise((resolve, reject) => {
      request(params, (err, res, body) => {
        resolve({ title: position.title, salary: body.salary });
      });
    });
  }

  static host(){
    console.log(process.env.SALARY_HOST)
    return process.env.SALARY_HOST || "http://localhost"
  }
}

module.exports = SalaryService;
