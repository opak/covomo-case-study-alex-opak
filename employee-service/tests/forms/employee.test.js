var EmployeeForm = require("../../forms/employee");
var EmployeeRepository = require("../../repositories/employee");

describe("test the form methods", () => {
  it("should transforms errors", () => {
    let errors = [{path: "firstName", message: "first name"}];
    let transformed = new EmployeeForm({}, {errors: errors});
    expect(transformed.errors.firstName).toEqual("first name");
  });

  it("should returns positions", () => {
    let transformed = new EmployeeForm({}, {});
    expect(transformed.positions).toEqual(EmployeeRepository.positions());
  });

  it("should filters positions", () => {
    let transformed = new EmployeeForm({ dataValues: { position: 0 } }, {});
    expect(transformed.filteredPositions).toEqual(
      EmployeeRepository.positions().filter(pos => pos.value !== 0)
    );
  });

  it("should filters positions", () => {
    let transformed = new EmployeeForm({ dataValues: { position: 0 } }, {});
    expect(transformed.filteredPositions).toEqual(
      EmployeeRepository.positions().filter(pos => pos.value !== 0)
    );
  });

  it("should returns selected position", () => {
    let transformed = new EmployeeForm({ dataValues: { position: 0 } }, {});
    expect(transformed.selectedPosition).toEqual(
      EmployeeRepository.positions().find(pos => pos.value === 0)
    );
  });
});
