var db = require("../../models");

var Employee = db.Employee;

describe("test the validations for Employee model", () => {
  let thisDb = db;
  beforeAll(async () => {
    await thisDb.sequelize.sync({ force: true });
  });

  it("should have validation for firstName", async () => {
    expect.assertions(1);
    let user = Employee.build({lastName: "last", position: 0});
    return user.validate().then(() => {}, (result) => {
      let error = result.errors.find(er => er.path === 'firstName');
      expect(error.message).toEqual("Please enter First Name");
    });
  });

  it("should have validation for lastName", async () => {
    expect.assertions(1);
    let user = Employee.build({firstName: "first", position: 0});
    return user.validate().then(() => {}, (result) => {
      let error = result.errors.find(er => er.path === 'lastName');
      expect(error.message).toEqual("Please enter Last Name");
    });
  });

  it("should have validation for position", async () => {
    expect.assertions(1);
    let user = Employee.build({firstName: "first", lastName: "last"});
    return user.validate().then(() => {}, (result) => {
      let error = result.errors.find(er => er.path === 'position');
      expect(error.message).toEqual("Please select Position");
    });
  });

  afterAll(async () => {
    await thisDb.sequelize.close();
  });
});
