var db = require("../../models");
var EmployeeRepository = require("../../repositories/employee");
var Employee = db.Employee;

describe("test positions", () => {
  it("should returns positions", () => {
    let positions = EmployeeRepository.positions();
    expect(positions).toEqual([
      { value: 0, title: "CEO", hoursRequired: false },
      { value: 1, title: "CTO", hoursRequired: false },
      { value: 2, title: "Head of Marketing", hoursRequired: false },
      { value: 3, title: "Freelancer", hoursRequired: true }
    ]);
  });

  it("should returns true if position requires hours", () => {
    let freelancer = 3;
    expect(EmployeeRepository.addHoursWorked(freelancer)).toEqual(true);
  });

  it("should returns false if position does't require hours", () => {
    let ceo = 0;
    expect(EmployeeRepository.addHoursWorked(ceo)).toEqual(false);
  });

  it("should returns false if position does't exist", () => {
    let outOfRange = 1000;
    expect(EmployeeRepository.addHoursWorked(outOfRange)).toEqual(false);
  });
});

describe("test .prepareParams", () => {
  it("should adds hoursWorked if position reqiures hours", () => {
    let freelancer = 3;
    let params = { position: freelancer, hoursWorked: "1" }
    EmployeeRepository.prepareParams(params);
    expect(params.hoursWorked).toEqual(1);
  });

  it("should removes hoursWorked if position doest reqiure hours", () => {
    let ceo = 0;
    let params = { position: ceo, hoursWorked: "1" }
    EmployeeRepository.prepareParams(params);
    expect(params.hoursWorked).toEqual(undefined);
  });
});

describe("test .new", () => {
  it("returns Employee instance", () => {
    let employee = EmployeeRepository.new({position: 0});
    expect(employee).toEqual(Employee.build({position: 0}));
  });
});

describe("test .update/.find/.all/.destroy", () => {
  let thisDb = db;
  beforeAll(async () => {
    await thisDb.sequelize.sync({ force: true });
  });

  it("updates model", async () => {
    expect.assertions(3);
    let employee = await Employee.create({firstName: "first", lastName: "last", position: 0});
    return EmployeeRepository.update(employee.id, {firstName: "updated"}).then((model) => {
      expect(model.dataValues.firstName).toEqual("updated");
      expect(model.dataValues.lastName).toEqual("last");
      expect(model.dataValues.position).toEqual(0);
    }, () => { });
  });

  it("finds model", async () => {
    expect.assertions(3);
    let employee = await Employee.create({firstName: "first", lastName: "last", position: 0});
    return EmployeeRepository.find(employee.id).then((model) => {
      expect(model.dataValues.firstName).toEqual("first");
      expect(model.dataValues.lastName).toEqual("last");
      expect(model.dataValues.position).toEqual(0);
    }, () => { });
  });

  it("destroyes model", async () => {
    expect.assertions(1);
    let employee = await Employee.create({firstName: "first", lastName: "last", position: 0});
    return EmployeeRepository.destroy(employee.id).then((model) => {
      return EmployeeRepository.find(employee.id).then((model) => {
        expect(model).toEqual(null);
      }, () => {});
    }, () => { });
  });

  it("returls model from all", async () => {
    await thisDb.sequelize.sync({ force: true });
    expect.assertions(1);
    let employee = await Employee.create({firstName: "first", lastName: "last", position: 0});
    return EmployeeRepository.all().then((models) => {
      expect(models.length).toEqual(1);
    }, () => { });
  });

  afterAll(async () => {
    await thisDb.sequelize.close();
  });
});
