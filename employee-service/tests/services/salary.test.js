var SalaryService = require("../../services/salary");

describe("Salary service", () => {
  it("should returns salary without hours", async () => {
    expect.assertions(2);
    SalaryService.makeRequest = (params) => {
      return new Promise((resolve, reject) => {
        resolve({title: "CEO", salary: 100000});
      });
    }
    const ceo = 0;
    SalaryService.get(ceo).then(salaryObj => {
      expect(salaryObj.title).toEqual("CEO");
      expect(salaryObj.salary).toEqual(100000);
    });
  });
});
