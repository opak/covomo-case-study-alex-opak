<?php

/** @var DI\Container $container
 */

$container->set(
    \App\Model\Salary\Repository\SalaryRepository::class,
    DI\get(\App\Infrastructure\Salary\ArrayRepository::class)
);
$container->set(
    \App\Application\Salary\SalaryCalculation\SalaryCalculationInterface::class,
    DI\get(\App\Application\Salary\SalaryCalculation\SalaryCalculationService::class)
);