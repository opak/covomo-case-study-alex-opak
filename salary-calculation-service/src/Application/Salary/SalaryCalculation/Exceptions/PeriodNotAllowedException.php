<?php


namespace App\Application\Salary\SalaryCalculation\Exceptions;


class PeriodNotAllowedException extends \Exception
{
    protected $message = 'Period not allowed!';
}
