<?php


namespace App\Application\Salary\SalaryCalculation\Exceptions;


class StrategyNotFoundException extends \Exception
{
    protected $message = 'Strategy not found!';
}
