<?php


namespace App\Application\Salary\SalaryCalculation;


interface SalaryCalculationInterface
{
    public function calculate(float $salary, float $period): float;

    public function setStrategy(string $strategy): self;
}