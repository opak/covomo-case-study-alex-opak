<?php


namespace App\Application\Salary\SalaryCalculation;


use App\Application\Salary\SalaryCalculation\Exceptions\StrategyNotFoundException;
use App\Model\Salary\SalaryType;

final class SalaryCalculationService implements SalaryCalculationInterface
{
    private SalaryCalculationStrategy $strategy;

    private const STRATEGY_IDENTITY_MAP = [
        SalaryType::HOURLY => HourlySalaryCalculationStrategy::class,
        SalaryType::YEARLY => YearlySalaryCalculationStrategy::class
    ];

    public function calculate(float $salary, float $period): float
    {
        return $this->strategy->calculate($salary, $period);
    }

    /**
     * @param string $strategy
     * @return SalaryCalculationService
     * @throws StrategyNotFoundException
     */
    public function setStrategy(string $strategy): self
    {
        $class = self::STRATEGY_IDENTITY_MAP[$strategy] ?? null;

        if (!$class) {
            throw new StrategyNotFoundException();
        }

        $this->strategy = new $class();
        return $this;
    }
}