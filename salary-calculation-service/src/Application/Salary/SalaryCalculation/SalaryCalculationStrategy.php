<?php


namespace App\Application\Salary\SalaryCalculation;


interface SalaryCalculationStrategy
{
    public function calculate(float $salary, float $period): float;
}