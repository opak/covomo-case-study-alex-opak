<?php


namespace App\Application\Salary\SalaryCalculation;


use App\Application\Salary\SalaryCalculation\Exceptions\PeriodNotAllowedException;

class YearlySalaryCalculationStrategy implements SalaryCalculationStrategy
{
    /**
     * @param float $salary
     * @param float $period
     * @return float
     * @throws PeriodNotAllowedException
     */
    public function calculate(float $salary, float $period): float
    {
        if($period){
            throw new PeriodNotAllowedException();
        }

        return $salary;
    }
}