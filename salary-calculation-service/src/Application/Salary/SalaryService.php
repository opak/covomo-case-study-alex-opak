<?php


namespace App\Application\Salary;


use App\Application\Salary\SalaryCalculation\SalaryCalculationInterface;
use App\Model\Salary\Repository\SalaryRepository;
use App\Model\Salary\Salary;
use App\Model\Salary\ValueObjects\Money;

class SalaryService
{

    private SalaryRepository $repository;
    private SalaryCalculationInterface $calculationService;

    public function __construct(SalaryRepository $repository, SalaryCalculationInterface $calculationService)
    {
        $this->repository = $repository;
        $this->calculationService = $calculationService;
    }

    public function calculateSalary(string $position, float $period): Salary
    {
        $salary = $this->repository->findByPosition($position);
        $calculated = $this->calculationService
            ->setStrategy($salary->type())
            ->calculate(
                $salary->money()->value(),
                $period
            );

        return new Salary(
            $salary->position(), $salary->type(), new Money($calculated, $salary->money()->currency())
        );
    }
}