<?php


namespace App\Infrastructure\Salary;


use App\Model\Salary\{Currency, Repository\SalaryRepository, Salary, SalaryType, ValueObjects\Money};
use DI\NotFoundException;

class ArrayRepository implements SalaryRepository
{
    private array $salaries = [
        ["position" => "CEO", "type" => SalaryType::YEARLY, "value" => 100000.0, "currency" => Currency::EUR],
        ["position" => "CTO", "type" => SalaryType::YEARLY, "value" => 85000.0, "currency" => Currency::EUR],
        ["position" => "Head of Marketing", "type" => SalaryType::YEARLY, "value" => 75000.0, "currency" => Currency::EUR],
        ["position" => "Freelancer", "type" => SalaryType::HOURLY, "value" => 50.0, "currency" => Currency::EUR],
    ];


    /**
     * @param string $position
     * @return Salary
     * @throws NotFoundException
     */
    public function findByPosition(string $position): Salary
    {
        foreach ($this->salaries as $salary) {
            if ($position === $salary['position']) {
                return new Salary(
                    $salary['position'],
                    $salary['type'],
                    new Money($salary['value'], $salary['currency'])
                );
            }
        }

        throw new NotFoundException($position . 'not found!');
    }
}
