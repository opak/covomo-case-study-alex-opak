<?php


namespace App;


use DI\Container;
use League\Route\RouteCollectionInterface;
use League\Route\Router;
use League\Route\Strategy\JsonStrategy;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\ResponseFactory;

final class Kernel
{
    private RequestHandlerInterface $requestHandler;

    private function buildRoutes(RouteCollectionInterface $route): void
    {
        require_once dirname(__DIR__) . '/config/routes.php';
    }

    private function buildServices(Container $container): void{
        require_once dirname(__DIR__) . '/config/services.php';
    }

    public function boot(): void
    {
        $containerBuilder = new \DI\ContainerBuilder();
        $containerBuilder->useAutowiring(true);
        $container = $containerBuilder->build();
        $this->buildServices($container);

        $strategy = (new JsonStrategy(new ResponseFactory()))->setContainer($container);
        $this->requestHandler = (new Router())->setStrategy($strategy);

        $this->buildRoutes($this->requestHandler);
    }

    public function dispatchRequest(ServerRequestInterface $request): ResponseInterface
    {
        return $this->requestHandler->handle($request);
    }
}