<?php


namespace App\Model\Salary\Repository;


use App\Model\Salary\Salary;

interface SalaryRepository
{
    public function findByPosition(string $position): Salary;
}