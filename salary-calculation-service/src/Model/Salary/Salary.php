<?php


namespace App\Model\Salary;


use App\Model\Salary\ValueObjects\Money;

final class Salary
{
    private string $position;
    private string $type;
    private Money $money;

    public function __construct(string $position, string $type, Money $money)
    {
        $this->position = $position;
        $this->type = $type;
        $this->money = $money;
    }

    /**
     * @return Money
     */
    public function money(): Money
    {
        return $this->money;
    }

    /**
     * @return string
     */
    public function position(): string
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }
}