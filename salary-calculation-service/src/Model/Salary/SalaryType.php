<?php


namespace App\Model\Salary;


interface SalaryType
{
    public const YEARLY = 'yearly';
    public const HOURLY = 'hourly';
}