<?php


namespace App\Model\Salary\ValueObjects;


final class Money
{
    private string $currency;
    private float $value;

    public function __construct(float $value, string $currency)
    {
        $this->value = $value;
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function value(): float
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function currency(): string
    {
        return $this->currency;
    }
}