<?php

namespace App\UI\Http\Salary;

use App\Application\Salary\SalaryCalculation\SalaryCalculationService;
use App\Application\Salary\SalaryCalculation\YearlySalaryCalculationStrategy;
use App\Application\Salary\SalaryService;
use App\Model\Salary\SalaryType;
use Psr\Http\Message\{ResponseInterface, ServerRequestInterface};
use Zend\Diactoros\Response;

class GetSalaryController
{
    /**
     * @var SalaryService
     */
    private SalaryService $salaryService;

    public function __construct(SalaryService $salaryService)
    {
        $this->salaryService = $salaryService;
    }

    /**
     * Controller.
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $salary = $this->salaryService->calculateSalary(
            $request->getQueryParams()['position'],
            $request->getQueryParams()['hours'] ?? 0
        );

        $response = new Response\JsonResponse(
            [
                'salary' => $salary->money()->value(),
                'per_hour' => SalaryType::HOURLY === $salary->type()
            ]
        );

        return $response->withStatus(200);
    }
}