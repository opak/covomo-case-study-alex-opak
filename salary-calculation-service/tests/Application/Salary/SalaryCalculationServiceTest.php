<?php


namespace App\Tests\Application\Salary;


use App\Application\Salary\SalaryCalculation\Exceptions\PeriodNotAllowedException;
use App\Application\Salary\SalaryCalculation\Exceptions\StrategyNotFoundException;
use App\Application\Salary\SalaryCalculation\SalaryCalculationService;
use App\Model\Salary\SalaryType;
use PHPUnit\Framework\TestCase;

class SalaryCalculationServiceTest extends TestCase
{
    public function testHourlySalary(): void
    {
        $service = new SalaryCalculationService();
        $calculated = $service->setStrategy(SalaryType::HOURLY)
            ->calculate(50, 10);

        $this->assertEquals(500, $calculated);
    }

    public function testHourlySalaryZeroPeriod(): void
    {
        $service = new SalaryCalculationService();
        $calculated = $service->setStrategy(SalaryType::HOURLY)
            ->calculate(50, 0);

        $this->assertEquals(0, $calculated);
    }

    public function testYearlySalary(): void
    {
        $service = new SalaryCalculationService();
        $calculated = $service->setStrategy(SalaryType::YEARLY)
            ->calculate(1000, 0);

        $this->assertEquals(1000.0, $calculated);
    }

    public function testYearlyPeriodNotAllowed(): void
    {
        $this->expectException(PeriodNotAllowedException::class);

        $service = new SalaryCalculationService();
         $service->setStrategy(SalaryType::YEARLY)
            ->calculate(1000, 1);
    }

    public function testNotFoundStrategy(): void
    {
        $this->expectException(StrategyNotFoundException::class);

        $service = new SalaryCalculationService();
        $service->setStrategy('strategy');

    }
}