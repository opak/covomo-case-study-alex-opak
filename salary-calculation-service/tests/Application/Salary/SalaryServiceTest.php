<?php


namespace App\Tests\Application\Salary;


use App\Application\Salary\SalaryCalculation\SalaryCalculationInterface;
use App\Application\Salary\SalaryService;
use App\Model\Salary\Currency;
use App\Model\Salary\Repository\SalaryRepository;
use App\Model\Salary\Salary;
use App\Model\Salary\SalaryType;
use App\Model\Salary\ValueObjects\Money;
use PHPUnit\Framework\TestCase;

class SalaryServiceTest extends TestCase
{
    public function getSalaryValues(): \Generator
    {
        $salary = new Salary('CEO', SalaryType::YEARLY, new Money(1000, Currency::EUR));

        $salaryCalculationService = $this->createMock(SalaryCalculationInterface::class);
        $salaryCalculationService->expects($this->once())
            ->method('setStrategy')
            ->willReturn($salaryCalculationService);
        $salaryCalculationService->expects($this->once())
            ->method('calculate')
            ->with(1000.0, 0)
            ->willReturn(1000.0);

        $salaryRepository = $this->createMock(SalaryRepository::class);
        $salaryRepository->expects($this->once())
            ->method('findByPosition')
            ->with('CEO')
            ->willReturn($salary);

        yield [$salary, $salaryCalculationService, $salaryRepository];
    }

    /**
     * @dataProvider getSalaryValues
     * @param Salary $salary
     * @param SalaryCalculationInterface $salaryCalculationService
     * @param SalaryRepository $salaryRepository
     */
    public function testGettingSalary(
        Salary $salary,
        SalaryCalculationInterface $salaryCalculationService,
        SalaryRepository $salaryRepository
    ) {
        $salaryService = new SalaryService($salaryRepository, $salaryCalculationService);
        $calculated = $salaryService->calculateSalary('CEO', 0);
        $this->assertEquals($calculated, $salary);
    }
}
