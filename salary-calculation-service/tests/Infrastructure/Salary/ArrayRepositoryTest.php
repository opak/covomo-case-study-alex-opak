<?php


namespace App\Tests\Infrastructure\Salary;


use App\Infrastructure\Salary\ArrayRepository;
use App\Model\Salary\Currency;
use App\Model\Salary\SalaryType;
use PHPUnit\Framework\TestCase;

class ArrayRepositoryTest extends TestCase
{
    private array $values = [
        ["position" => "CEO", "type" => SalaryType::YEARLY, "value" => 1000.0, "currency" => Currency::EUR],
        ["position" => "CTO", "type" => SalaryType::HOURLY, "value" => 75.0, "currency" => Currency::EUR],
    ];

    public function testFindByName()
    {
        $repository = new ArrayRepository();

        $reflection = new \ReflectionClass($repository);
        $reflectionProperty = $reflection->getProperty('salaries');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($repository, $this->values);

        $salary = $repository->findByPosition('CEO');
        $this->assertEquals(1000, $salary->money()->value());
        $this->assertEquals('CEO', $salary->position());
        $this->assertEquals(SalaryType::YEARLY, $salary->type());
    }
}
