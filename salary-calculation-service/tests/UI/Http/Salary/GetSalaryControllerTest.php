<?php


namespace App\Tests\UI\Http\Salary;


use App\Application\Salary\SalaryService;
use App\Model\Salary\Currency;
use App\Model\Salary\Salary;
use App\Model\Salary\SalaryType;
use App\Model\Salary\ValueObjects\Money;
use App\UI\Http\Salary\GetSalaryController;
use PHPUnit\Framework\TestCase;
use Zend\Diactoros\ServerRequestFactory;

class GetSalaryControllerTest extends TestCase
{
    public function testGetYearly(): void
    {
        $request = ServerRequestFactory::fromGlobals($_SERVER, ['position' => 'CEO']);
        $salaryServiceMock = $this->createMock(SalaryService::class);
        $salaryServiceMock->expects($this->once())
            ->method('calculateSalary')
            ->willReturn(new Salary('CEO', SalaryType::YEARLY, new Money(100, Currency::EUR)));

        $controller = new GetSalaryController($salaryServiceMock);
        $response = $controller($request);

       $data =  json_decode($response->getBody()->getContents(), true);

       $this->assertEquals(['salary' => 100, 'per_hour' => false], $data);
    }
}
